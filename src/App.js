// import logo from "./logo.svg";
import "./App.css";
import CartShoesRedux from "./CartShoesRedux/CartShoesRedux";

function App() {
  return (
    <div className="App">
      <CartShoesRedux />
    </div>
  );
}

export default App;
