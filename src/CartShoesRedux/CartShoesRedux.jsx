import React, { Component } from "react";
import { connect } from "react-redux";
import GioHang from "./GioHang";
import ItemShoe from "./ItemShoe";
import { ADD_TO_CART, CHANGE_VALUE } from "./redux/constants/shoeConstant";

class CartShoesRedux extends Component {
  renderContent = () => {
    return this.props.listShoe.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          handleAddToCart={this.props.handleAddToCart}
          data={item}
        />
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        {this.props.cart.length > 0 && (
          <GioHang handleChangeQuantity={this.props.handleChangeQuantity} />
        )}
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listShoe: state.shoeReducer.shoes,
    cart: state.shoeReducer.gioHang,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
    handleChangeQuantity: (id, value) => {
      dispatch({
        type: CHANGE_VALUE,
        payload: { id, value },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartShoesRedux);
