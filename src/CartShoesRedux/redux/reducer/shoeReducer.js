import { data_shoes } from "../../data_shoes";
import { ADD_TO_CART, CHANGE_VALUE } from "../constants/shoeConstant";

let initialState = {
  shoes: data_shoes,
  gioHang: [],
};

export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.gioHang.findIndex((item) => {
        return item.id === payload.id;
      });

      let cloneGioHang = [...state.gioHang];

      if (index === -1) {
        let newSp = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSp);
      } else {
        cloneGioHang[index].soLuong++;
      }

      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case CHANGE_VALUE: {
      let index = state.gioHang.findIndex((item) => {
        return item.id === payload.id;
      });
      let cloneGioHang = [...state.gioHang];

      if (index === -1) return;
      cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + payload.value;
      cloneGioHang[index].soLuong === 0 && cloneGioHang.splice(index, 1);
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
